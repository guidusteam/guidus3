﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Bullet : MonoBehaviour {

    public Transform shoot_point;
    public bool in_use;
    MeshRenderer draw;
    SphereCollider collider;
    public TrailRenderer trail;
    public ParticleSystem hit_eff;
    public ParticleSystem exp_eff;
    Transform hit_eff_transform;
    Transform bullet_trans;

    void Start()
    {
        draw = GetComponent<MeshRenderer>();
        collider = GetComponent<SphereCollider>();
        draw.enabled = false;
        collider.enabled = false;
        trail = GetComponent<TrailRenderer>();
        in_use = false;
        hit_eff_transform = hit_eff.transform;
        bullet_trans = transform;
    }

    void OnTriggerEnter(Collider other)
    {
        //적이나 장애물에 충돌하면 제자리로,   총알끼리, 문에 충돌하면 무시!
        if (other.CompareTag("Obstacle") || other.CompareTag("Collider_Wall"))
            StartCoroutine("Reset");
        else if (other.CompareTag("Enemy"))
        {
            //=============총알타입=============
            //일반
            if (PlayerController.Instance.player_Data.bullet_type == BulletType.normal)
            {
                Sound_Manager.Instance.EffPlay("hit");
                hit_eff_transform.position = bullet_trans.position;
                hit_eff.Play();
            }

            //확률로 골드
            else if (PlayerController.Instance.player_Data.bullet_type == BulletType.drop_gold)
            {
                Sound_Manager.Instance.EffPlay("hit");
                hit_eff_transform.position = bullet_trans.position;
                hit_eff.Play();

                int num = Random.Range(0, 100);
                if (num <= 20) PlayerController.Instance.player_State.Update_gold(5);
            }

            //확률로 체력 회복
            else if (PlayerController.Instance.player_Data.bullet_type == BulletType.get_hp)
            {
                Sound_Manager.Instance.EffPlay("hit");
                hit_eff_transform.position = bullet_trans.position;
                hit_eff.Play();

                int num = Random.Range(0, 100);
                if (num <= 20) PlayerController.Instance.player_State.Update_hp(2);
            }
            //폭발
            else if (PlayerController.Instance.player_Data.bullet_type == BulletType.explosion)
            {
                exp_eff.gameObject.SetActive(true);
                exp_eff.transform.position = bullet_trans.position;
                exp_eff.Play();
            }

            StartCoroutine("Reset");
        }
    }

    public void SetActive()
    {
        in_use = true;
        draw.enabled = true;
        collider.enabled = true;
        trail.enabled = true;
    }

    IEnumerator Reset()
    {
        draw.enabled = false;
        collider.radius = 0.5f;
        collider.enabled = false;
        trail.enabled = false;
        bullet_trans.DOKill();

        yield return new WaitForSeconds(0.5f);

        bullet_trans.position = shoot_point.position;
        exp_eff.gameObject.SetActive(false);
        in_use = false;
    }
}
