﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public class WWW_Class : MonoBehaviour {
    
    Intro intro;
    const string receive_familyname = "http://161.202.110.157/guidus/receive_familyname.php";
    const string judge_familyname = "http://161.202.110.157/guidus/judge_familyname.php";
    const string join = "http://161.202.110.157/guidus/join.php";
    const string receive_value = "http://161.202.110.157/guidus/receive_value.php";

    void Start()
    {
        //StartCoroutine("Call", url);
        intro = GetComponent<Intro>();
    }

    IEnumerator Setting()
    {
        Fade.Instance.FadeOut();
        Fade.Instance.StartCoroutine("ShowIndicator", true);

        WWWForm cForm = new WWWForm();

        cForm.AddField("UID", SystemInfo.deviceUniqueIdentifier);

        WWW wwwUrl = new WWW(join, cForm);

        yield return wwwUrl;

        Debug.Log("join: " + wwwUrl.text);
        string[] join_msg = (wwwUrl.text).Split(':');
        // if (join_msg[0] == "ok")
        // {
        cForm = new WWWForm();
        cForm.AddField("UID", SystemInfo.deviceUniqueIdentifier);

        wwwUrl = new WWW(judge_familyname, cForm);

        yield return wwwUrl;

        Debug.Log(("judge:"+ wwwUrl.text));

        string[] msg = (wwwUrl.text).Split(':');
        
        //등록된 기기
        if (msg[0] == "ok") //ok:가문이름
        {
            PlayerController.Instance.player_State.name = msg[1].Substring(0, msg[1].Length - 5);
            intro.setting = true;
            intro.ShowIntro();
        }

        //처음 시작
        else//error:에러문구
        {
            intro.setting_ui.SetActive(true);
        }
        // }
        // else //DB에 존재함
        // {
        //     Debug.Log("DB저장 실패");
        // }
        Fade.Instance.HideIndicator();
        Fade.Instance.FadeIn();
    }

  //  IEnumerator SendDefaultValue()
  //  {
  //      WWWForm cform = new WWWForm();
  //      cform.AddField("hp", Define.MAX_HEALTH);
  //
  //      Fade.instance.HideIndicator();
  //      Fade.instance.FadeIn();
  //  }

    IEnumerator SendFName(string fname)
    {
        Fade.Instance.FadeOut();
        Fade.Instance.StartCoroutine("ShowIndicator", true);
        WWWForm form = new WWWForm();
        form.AddField("UID", SystemInfo.deviceUniqueIdentifier);
        form.AddField("fName", fname + SystemInfo.deviceUniqueIdentifier.Substring(0, 5));
        print("가문:"+fname + SystemInfo.deviceUniqueIdentifier.Substring(0, 5));
        WWW wwwUrl = new WWW(receive_familyname, form);

        yield return wwwUrl;

        Debug.Log("sendfname: " + wwwUrl.text);

        Fade.Instance.HideIndicator();
        Fade.Instance.FadeIn();

        PlayerController.Instance.player_State.name = fname;
        intro.setting = true;
        intro.ShowIntro();
    }
}
