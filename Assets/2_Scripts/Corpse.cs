﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Corpse : MonoBehaviour {

    public ParticleSystem corpse;
    public GameObject obj;
    GameObject barricades;
    Maps maps;
    Show_State show_state;
    BoxCollider collider;
    public int cor_room = -1;  //방 번호
    bool able_to_touch = false;

    void Start()
    {
        barricades = GameObject.Find("Barricade");
        maps = GameObject.Find("Maps").GetComponent<Maps>();
        show_state = GameObject.Find("Manager").GetComponent<Show_State>();

        collider = GetComponent<BoxCollider>();

        collider.enabled = false;
        obj.SetActive(false);
    }
    
    public void SetPosition()
    {
        cor_room = Random.Range(1, Define.ROOM_NUM - 2);
    }      

    public void Active()
    {
        if(maps.room_num == cor_room)
        {
            //맵에 보이게
            obj.SetActive(true);
            collider.enabled = true;

            corpse.Play();
        }
        else
        {
            obj.SetActive(false);
            collider.enabled = false;
        }
    }

    public void RemoveBarricades()
    {
        if (maps.room_num == cor_room)
        {
            barricades.transform.DOMoveY(-3, 0.5f);
            able_to_touch = true;
        }
    }

    void OnMouseDown()
    {
        //동작
        if(able_to_touch && Vector3.Distance(transform.position, PlayerController.Instance.player_Transform.position) <= 3)
        {
            StartCoroutine("PickUp");
        }
    }

    IEnumerator PickUp()
    {
        PlayerController.Instance.player_Animate.player_anim.CrossFade("pickup", 0.3f);

        yield return new WaitForSeconds(0.5f);

        PlayerController.Instance.player_State.SetValue(PlayerController.Instance.player_Revive.result[0], PlayerController.Instance.player_Revive.result[1],
        PlayerController.Instance.player_Revive.result[2], PlayerController.Instance.player_Revive.result[3], PlayerController.Instance.player_Revive.result[4]);
        PlayerController.Instance.player_State.ChangedValueReset();

        for(int i = 0; i <5; ++i)
        {
            if (PlayerController.Instance.player_Revive.result[i] > 0)
            {
                string type = " ";
                if (i == 0) type = "Max HP ";
                else if (i == 1) type = "Max Stamina ";
                else if (i == 2) type = "Attack Power ";
                else if (i == 3) type = "Move Speed ";
                else if (i == 4) type = "Shoot Speed ";
                PlayerController.Instance.player_State.head_3d.text = type + PlayerController.Instance.player_Revive.result[i].ToString();
                show_state.StartCoroutine("ShowHeadText", PlayerController.Instance.player_State.head_3d);
            }
        }

        PlayerController.Instance.player_State.Update_hp();
        PlayerController.Instance.player_State.Update_st();
        PlayerController.Instance.player_State.Update_ui();

        cor_room = -1;

        //제자리 후 없애기
        barricades.transform.DOMoveY(0, 0);
        collider.enabled = false;
        obj.SetActive(false);

        PlayerController.Instance.player_Animate.Reset();
    }
}
