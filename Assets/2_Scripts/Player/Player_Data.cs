﻿using UnityEngine;
using System.Collections;

public class Player_Data : MonoBehaviour {
    public string name;
    public int floor = 99, generation = 1;
    public int gold = 10000;

    public float health, stamina;
    public float max_health = Define.MAX_HEALTH, max_stamina = Define.MAX_STAMINA;
    public float move_speed = Define.MOVE_SPEED, shoot_speed = Define.SHOOT_SPEED;
    public float attack_power = Define.ATTACK_POWER;
    public int bullet_dist = 50;
    public ShootPath shoot_path = ShootPath.linear;
    public BulletType bullet_type = BulletType.normal;
    public BulletNum bullet_num = BulletNum.one;

    //===================items===================
    public bool zombie = false;
    public bool map = false;
    //===========================================

    public struct Changed
    {
        public float max_health, max_stamina;
        public float move_speed, shoot_speed;
        public float attack_power;
    }

    public Changed changed;

    public void LoadState()
    {
        //값있음
        if (PlayerPrefs.HasKey("name"))
        {
            name = PlayerPrefs.GetString("name");
            floor = PlayerPrefs.GetInt("floor");
            generation = PlayerPrefs.GetInt("generation");
            gold = PlayerPrefs.GetInt("gold");
            max_health = PlayerPrefs.GetFloat("max_hp");
            max_stamina = PlayerPrefs.GetFloat("max_st");
            move_speed = PlayerPrefs.GetFloat("move_spd");
            shoot_speed = PlayerPrefs.GetFloat("shoot_spd");
            attack_power = PlayerPrefs.GetFloat("att_power");
        }

        //처음
        else
        {
            name = "락락";
            floor = 99;
            generation = 1;
            gold = 10000;
            max_health = Define.MAX_HEALTH; max_stamina = Define.MAX_STAMINA;
            move_speed = Define.MOVE_SPEED; shoot_speed = Define.SHOOT_SPEED;
            attack_power = Define.ATTACK_POWER;
            SaveState();
        }
    }

    public void SaveState()
    {
        PlayerPrefs.SetString("name", name);
        PlayerPrefs.SetInt("floor", floor);
        PlayerPrefs.SetInt("generation", generation);
        PlayerPrefs.SetInt("gold", gold);
        PlayerPrefs.SetFloat("max_hp", max_health);
        PlayerPrefs.SetFloat("max_st", max_stamina);
        PlayerPrefs.SetFloat("move_spd", move_speed);
        PlayerPrefs.SetFloat("shoot_spd", shoot_speed);
        PlayerPrefs.SetFloat("att_power", attack_power);
    }

}
