﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Player_Revive : MonoBehaviour {
    
    Text hp_v, stam_v, att_v, mspd_v, sspd_v;
    public Text gold;
    public Text restart;
    public Text[] percent_t;
    public Text[] result_t;
    public GameObject buttons;
    int[] percent;
    public float[] result;
    GameObject pop_up;
    CanvasGroup popup_cvs;
    CanvasGroup button_cvs;
    Caravan caravan;
    public Corpse corpse;
    Maps maps;

    int cost = Define.ROLL_COST;
    bool retry = false;

	void Start () {
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
        maps = GameObject.Find("Maps").GetComponent<Maps>();

        hp_v = GameObject.Find("hp_value").GetComponent<Text>();
        stam_v = GameObject.Find("stamina_value").GetComponent<Text>();
        att_v = GameObject.Find("attack_value").GetComponent<Text>();
        mspd_v = GameObject.Find("mspeed_value").GetComponent<Text>();
        sspd_v = GameObject.Find("sspeed_value").GetComponent<Text>();

        pop_up = GameObject.Find("Pop-up");
        popup_cvs = pop_up.GetComponent<CanvasGroup>();
        button_cvs = buttons.GetComponent<CanvasGroup>();
        buttons.SetActive(false);
        
        pop_up.SetActive(false);

        percent = new int[5];
        result = new float[5];
	}

    public void Setting()   //랜덤퍼센트
    {
        for (int i = 0; i < 5; ++i)
        {
            int n = Random.Range(0, Define.MAX_PERCENT);
            percent[i] = n;
            percent_t[i].text = "+ "+n.ToString() + "%  =";
        }

        CalculateResult();
    }

    public void Roll()
    {
        //다시 돌릴땐 2배
        if (retry) cost = cost * 2;
        retry = true;

        //금액없으면 못돌리게
        if (PlayerController.Instance.player_Data.gold - cost < 0) return;

        //text 제자리      //결과 없애기
        for (int i = 0; i < 5; ++i)
        {
            percent_t[i].gameObject.transform.DOLocalMove(new Vector3(820, 800, -2000), 0);
            result_t[i].DOFade(0, 0);
        }

        //버튼
        Fade.Instance.FadeOut_group(button_cvs, 0);
        buttons.SetActive(false);

        PlayerController.Instance.player_Data.gold -= cost;

        Setting();

        StartCoroutine("ShowWindow",false);
    }

    void CalculateResult()
    {
        result[0] = PlayerController.Instance.player_Data.changed.max_health * percent[0] * 0.01f;
        result[1] = PlayerController.Instance.player_Data.changed.max_stamina * percent[1] * 0.01f;
        result[2] = PlayerController.Instance.player_Data.changed.attack_power * percent[2] * 0.01f;
        result[3] = PlayerController.Instance.player_Data.changed.move_speed * percent[3] * 0.01f;
        result[4] = PlayerController.Instance.player_Data.changed.shoot_speed * percent[4] * 0.01f;

        result_t[0].text = result[0].ToString();
        result_t[1].text = result[1].ToString();
        result_t[2].text = result[2].ToString();
        result_t[3].text = (-result[3] * 100).ToString();
        result_t[4].text = (-result[4] * 100).ToString();
    }

    IEnumerator ShowWindow(bool first)
    {
        yield return new WaitForSeconds(0.5f);
        if (first)  //처음 돌려서 팝업띄운 경우
        {

            pop_up.SetActive(true);

            restart.text = (PlayerController.Instance.player_Data.generation+ 1).ToString() + "대 시작";
            gold.text = "Gold : " + PlayerController.Instance.player_Data.gold;
            hp_v.text = PlayerController.Instance.player_Data.changed.max_health.ToString();
            stam_v.text = PlayerController.Instance.player_Data.changed.max_stamina.ToString();
            att_v.text = PlayerController.Instance.player_Data.changed.attack_power.ToString();
            mspd_v.text = (-PlayerController.Instance.player_Data.changed.move_speed * 100).ToString();
            sspd_v.text = (-PlayerController.Instance.player_Data.changed.shoot_speed * 100).ToString();
            
            Fade.Instance.FadeIn_group(popup_cvs);

            yield return new WaitForSeconds(0.5f);
        }

        gold.text = "Gold : " + PlayerController.Instance.player_Data.gold;
        
        //percent날아오게
        for (int i = 0; i < 5; ++i)
        {
            percent_t[i].gameObject.transform.DOLocalMove(new Vector3(420, 0, 0), 0.3f);
            yield return new WaitForSeconds(0.3f);
        }

        //결과값 출력
        for (int i = 0; i < 5; ++i)
            result_t[i].DOFade(1, 0.5f);

        yield return new WaitForSeconds(0.5f);

        //버튼 활성
        buttons.SetActive(true);
        Fade.Instance.FadeIn_group(button_cvs);
    }

    public void Restart()
    {
        retry = false;

        //얻은 수치 적용
        PlayerController.Instance.player_State.SetValue(result[0], result[1], result[2], result[3], result[4]);
        
        //text 제자리      //결과 없애기
        for (int i = 0; i < 5; ++i)
        {
            percent_t[i].gameObject.transform.DOLocalMove(new Vector3(820, 800, -2000), 0);
            result_t[i].DOFade(0, 0);
        }
        
        //버튼
        Fade.Instance.FadeOut_group(button_cvs, 0);
        buttons.SetActive(false);

        //플레이어 체력, 스테미너 
        PlayerController.Instance.player_State.Update_hp(PlayerController.Instance.player_Data.max_health);
        PlayerController.Instance.player_State.Update_st(PlayerController.Instance.player_Data.max_stamina);
        PlayerController.Instance.player_State.Update_ui();

        //세대증가
        PlayerController.Instance.player_Data.generation++;

        pop_up.SetActive(false);


        //시체 위치
        corpse.SetPosition();


        //caravan이동
        maps.SendMessage("Reset");
        Application.LoadLevel("Caravan");
    }
}