﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Player_Collision : MonoBehaviour {
    
    Item_Manager item_mng;
    Caravan caravan;
    Maps maps;

    void Start()
    {
        if (Application.loadedLevelName == "Game(0~10)")
        {
            item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
            maps = GameObject.Find("Maps").GetComponent<Maps>();
        }
        else if(Application.loadedLevelName == "Caravan")
            caravan = GameObject.Find("Manager").GetComponent<Caravan>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (PlayerController.Instance.player_Data.zombie) return;

        switch (other.tag)
        {
            case "Worm_Bullet":
                PlayerController.Instance.player_State.Update_hp(-3);
                break;

            case "Arrow":
                PlayerController.Instance.player_State.Update_hp(-5);
                other.gameObject.SetActive(false);
                break;

            case "Coin":
                StartCoroutine("PickUp", other);
                break;

            case "HP":
                StartCoroutine("PickUp", other);
                break;

            case "Left":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.left])
                    maps.MoveMap((int)Dir.left);
                break;

            case "Right":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.right])
                    maps.MoveMap((int)Dir.right);
                break;

            case "Up":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.up])
                    maps.MoveMap((int)Dir.up);
                break;

            case "Down":
                if (maps.door_scrt[maps.room_num].isOpened[(int)Dir.down])
                    maps.MoveMap((int)Dir.down);
                break;

            case "Exit":
                caravan.StartCoroutine("StartGame");
                break;

            case "Obstacle":
                transform.DOFlip();
                break;

            case "NPC":
                break;
        }
    }
   
    IEnumerator PickUp(Collider obj)
    {
        PlayerController.Instance.player_Animate.player_anim.CrossFade("pickup",0.3f);
   
        yield return new WaitForSeconds(0.5f);
   
        if (obj.CompareTag("Coin"))
            PlayerController.Instance.player_State.Update_gold(10);
        else if (obj.CompareTag("HP"))
            PlayerController.Instance.player_State.Update_hp(10);
   
        item_mng.HideItem(maps.room_num);
        PlayerController.Instance.player_Animate.Reset();
    }
}
