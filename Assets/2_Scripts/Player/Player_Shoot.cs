﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player_Shoot : MonoBehaviour
{
    public Animation player_anim;
    GameObject[] bullet;
    GameObject[] grenade;
    Transform[] bullet_trans;
    Bullet[] bullet_scrt;
    TrailRenderer[] trail;
    Grenade[] grenade_scrt;

    //==============================
    int bullet_num = 30;
    int curve_dist = 4;      //파장  작을수록
    int curve = 2;           //진폭  클수록 크게 진동
    public int shoot_num = 1;
    //==============================
    float shoot_time = -1;
    float anim_time = 0;
    Vector3 shoot_dir;

    public Camera main_camera;
    Ray ray;
    RaycastHit hit;
    void Start()
    {
        bullet_trans = new Transform[bullet_num];
        bullet_scrt = new Bullet[bullet_num];
        trail = new TrailRenderer[bullet_num];
        bullet = new GameObject[bullet_num];
        bullet = GameObject.FindGameObjectsWithTag("Bullet");
        grenade = new GameObject[bullet_num];
        grenade = GameObject.FindGameObjectsWithTag("Grenade");
        grenade_scrt = new Grenade[bullet_num];

        anim_time = player_anim.GetClip("attack").length;

        for (int i = 0; i < bullet_num; ++i)
        {
            bullet_scrt[i] = bullet[i].GetComponent<Bullet>();
            bullet_trans[i] = bullet[i].GetComponent<Transform>();
            trail[i] = bullet[i].GetComponent<TrailRenderer>();
            grenade_scrt[i] = grenade[i].GetComponent<Grenade>();
            grenade[i].SetActive(false);
        }
    }

    public void ShootBullet(Vector3 dir) {
        if (Time.time - shoot_time > anim_time)
        {
            shoot_dir = dir;
            shoot_time = Time.time;
            for (int i = 0; i < bullet_num; ++i)
            {
                if (!bullet_scrt[i].in_use)
                {
                    bullet_scrt[i].SetActive();
                    Sound_Manager.Instance.EffPlay("shoot");

                    if (PlayerController.Instance.player_Data.shoot_path == ShootPath.linear)
                        LinearShoot(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.curve)
                        CurveShoot(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.spiral)
                        SpiralShoot(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.beam)
                        Beam(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.chasing)
                        ChasingShoot(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.parabola)
                        ParabolaShoot(i);
                    else if (PlayerController.Instance.player_Data.shoot_path == ShootPath.random)
                    {
                        int path = Random.Range(0, 5);
                        if ((ShootPath)path == ShootPath.linear) LinearShoot(i);
                        else if ((ShootPath)path == ShootPath.curve) CurveShoot(i);
                        else if ((ShootPath)path == ShootPath.spiral) SpiralShoot(i);
                        else if ((ShootPath)path == ShootPath.beam) Beam(i);
                        else if ((ShootPath)path == ShootPath.chasing) ChasingShoot(i);
                    }
                    return;
                }
            }
        }
    }

    public void SetBulletType(BulletType type)
    {
        if (type == BulletType.big_slow)
        {
            for (int i = 0; i < bullet_trans.Length; ++i)
            {
                bullet_trans[i].DOScale(0.5f, 0);
                bullet_scrt[i].trail.startWidth = 0.7f;
            }

            player_anim["attack"].speed = 0.7f;
            anim_time = player_anim.GetClip("attack").length / 0.7f;
            print(anim_time);
        }
        else if (type == BulletType.small_fast)
        {
            for (int i = 0; i < bullet_trans.Length; ++i)
            {
                bullet_trans[i].DOScale(0.2f, 0);
                bullet_scrt[i].trail.startWidth = 0.1f;

            }

            player_anim["attack"].speed = 1.3f;
            anim_time = player_anim.GetClip("attack").length / 1.3f;
            print(anim_time);
        }
        else
        {
            for (int i = 0; i < bullet_trans.Length; ++i)
            {
                bullet_trans[i].DOScale(0.3f, 0);
                bullet_scrt[i].trail.startWidth = 0.2f;
                player_anim["attack"].speed = 1.0f;
                anim_time = player_anim.GetClip("attack").length;
            }
        }
    }

    void LinearShoot(int num)
    {
        bullet_trans[num].position = transform.position;
        bullet_trans[num].DOMove(transform.position + shoot_dir * PlayerController.Instance.player_Data.bullet_dist, PlayerController.Instance.player_Data.shoot_speed).SetEase(Ease.Linear).OnComplete(() => Reset(num));
    }

    void CurveShoot(int num)
    {
        //경로 찍기
        bullet_trans[num].transform.position = transform.position;
        Vector3 dir = Vector3.Cross(shoot_dir, Vector3.up).normalized;

        Vector3[] path = new Vector3[PlayerController.Instance.player_Data.bullet_dist / curve_dist];

        for (int i = 0; i < PlayerController.Instance.player_Data.bullet_dist / curve_dist; ++i)
        {
            if (i % 2 == 0) path[i] = bullet_trans[num].position + dir * curve + shoot_dir * curve_dist * i;
            else path[i] = bullet_trans[num].position - dir * curve + shoot_dir * curve_dist * i;
        }
        bullet_trans[num].DOPath(path, PlayerController.Instance.player_Data.shoot_speed, PathType.CatmullRom).SetEase(Ease.Linear).OnComplete(() => Reset(num));
    }

    void SpiralShoot(int num)
    {
        bullet_trans[num].position = transform.position;
        bullet_trans[num].DOSpiral(PlayerController.Instance.player_Data.shoot_speed * 2, Vector3.up, SpiralMode.Expand, 1, 2).OnComplete(() => Reset(num));
    }

    void Beam(int num)
    {
        bullet_scrt[num + 1].SetActive();

        bullet_trans[num].position = transform.position;
        bullet_trans[num + 1].position = transform.position;

        bullet_trans[num].DOMove(transform.position + shoot_dir * PlayerController.Instance.player_Data.bullet_dist, PlayerController.Instance.player_Data.shoot_speed * 0.1f).SetEase(Ease.Linear).OnComplete(() => Reset(num));
        bullet_trans[num + 1].DOMove(transform.position + -shoot_dir * PlayerController.Instance.player_Data.bullet_dist, PlayerController.Instance.player_Data.shoot_speed * 0.1f).SetEase(Ease.Linear).OnComplete(() => Reset(num + 1));
    }

    void ChasingShoot(int num)
    {
        ;
    }

    void ParabolaShoot(int num)
    {
        Vector3 dest = shoot_dir * 10 + transform.position;
        dest = new Vector3(dest.x, 0, dest.z);  //바닥에 닿게

        bullet_trans[num].position = transform.position;

        //포물선이동 -> 수류탄설치
        bullet_trans[num].DOJump(dest, 5, 1, PlayerController.Instance.player_Data.shoot_speed)
            .SetEase(Ease.OutCubic).OnComplete(() => InstallGrenade(num, dest));
    }

    void InstallGrenade(int num, Vector3 dest)
    {
        grenade[num].SetActive(true);
        grenade[num].transform.position = dest;

        grenade_scrt[num].StartCoroutine("Explode", dest);

        Reset(num);
    }

    void Reset(int b_num)
    {
        bullet_scrt[b_num].StartCoroutine("Reset");
    }
}