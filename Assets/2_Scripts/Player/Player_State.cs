﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Player_State : MonoBehaviour {

    
    public Controller controller;
    public Maps maps;
    public Show_State show_state;
    public WWW_Class www;

    public Text die_msg;
    public TextMesh head_3d;
    public Animation player_anim;
    public ParticleSystem die_eff;
    public CanvasGroup game_ui;
    public Image game_over;
    public Dropdown bullet_type;
    public Dropdown shoot_path;
    
  

    public void StartGame()
    {
        PlayerController.Instance.player_Data.LoadState();

        PlayerController.Instance.player_Data.changed.attack_power = 0;
        PlayerController.Instance.player_Data.changed.max_health = 0;
        PlayerController.Instance.player_Data.changed.max_stamina = 0;
        PlayerController.Instance.player_Data.changed.move_speed = 0;
        PlayerController.Instance.player_Data.changed.shoot_speed = 0;
        PlayerController.Instance.player_Data.health = PlayerController.Instance.player_Data.max_health;
        PlayerController.Instance.player_Data.stamina = PlayerController.Instance.player_Data.max_stamina;

        StartCoroutine("Update_State");
    }

    IEnumerator Update_State()            //0.1초
    {
        //머리위 텍스트 회전 고정
        head_3d.transform.rotation = Quaternion.Euler(40, 0, 0);

        //스테미너 증가
        Update_st(0.1f, false);

        //좀비상태
        if (PlayerController.Instance.player_Data.zombie)
            Update_hp(-0.02f, false, false);


        yield return new WaitForSeconds(0.1f);
        StartCoroutine("Update_State");
    }

    public void Update_hp(float num = 0, bool show_text = true, bool attack = true)
    {
        //좀비상태에서 공격받았을 때 체력 감소X
        if (attack && PlayerController.Instance.player_Data.zombie) return;

        PlayerController.Instance.player_Data.health += num;
        PlayerController.Instance.player_Data.health = 
            Mathf.Clamp(PlayerController.Instance.player_Data.health, 0, PlayerController.Instance.player_Data.max_health + PlayerController.Instance.player_Data.changed.max_health);
        show_state.Update_HP();

        if (num != 0 && show_text)
        {
            head_3d.text = "HP  " + num.ToString();
            show_state.StartCoroutine("ShowHeadText", head_3d);
        }

        //죽음
        if (PlayerController.Instance.player_Data.health <= 0)
            StartCoroutine("Die");
    }

    public void Update_gold(int num)
    {
        PlayerController.Instance.player_Data.gold += num;
              
        head_3d.text = "GOLD  " + num.ToString();
        show_state.StartCoroutine("ShowHeadText", head_3d);
    }

    public void Update_st(float num = 0, bool show_text = true)
    {
        PlayerController.Instance.player_Data.stamina += num;

        PlayerController.Instance.player_Data.stamina = Mathf.Clamp(PlayerController.Instance.player_Data.stamina, 0, PlayerController.Instance.player_Data.max_stamina + PlayerController.Instance.player_Data.changed.max_stamina);
        show_state.Update_ST();

        if (num != 0 && show_text)
        {
            head_3d.text = "STAMINA  " + num.ToString();
            show_state.StartCoroutine("ShowHeadText", head_3d);
        }
    }

    public void Update_ui()
    {
        show_state.SendMessage("UpdateAbility");
        show_state.SendMessage("UpdateFloor");
    }

    public void SetBulletType()
    {
        switch (bullet_type.options[bullet_type.value].text)
        {
            case "Normal":
                PlayerController.Instance.player_Data.bullet_type = BulletType.normal;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.normal);
                break;
            case "Gold":
                PlayerController.Instance.player_Data.bullet_type = BulletType.drop_gold;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.drop_gold);
                break;
            case "Poison":
                PlayerController.Instance.player_Data.bullet_type = BulletType.poison;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.poison);
                break;
            case "Freeze":
                PlayerController.Instance.player_Data.bullet_type = BulletType.freeze;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.freeze);
                break;
            case "Explosion":
                PlayerController.Instance.player_Data.bullet_type = BulletType.explosion;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.explosion);
                break;
            case "HP":
                PlayerController.Instance.player_Data.bullet_type = BulletType.get_hp;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.get_hp);
                break;
            case "SmallFast":
                PlayerController.Instance.player_Data.bullet_type = BulletType.small_fast;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.small_fast);
                break;
            case "BigSlow":
                PlayerController.Instance.player_Data.bullet_type = BulletType.big_slow;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.big_slow);
                break;
            case "Grenade":
                PlayerController.Instance.player_Data.bullet_type = BulletType.grenade;
                PlayerController.Instance.player_Data.shoot_path = ShootPath.parabola;
                PlayerController.Instance.player_Shoot.SetBulletType(BulletType.grenade);
                break;
        }
    }

    public void SetShootPath()
    {
        if (PlayerController.Instance.player_Data.bullet_type == BulletType.grenade)
            return;
        switch (shoot_path.options[shoot_path.value].text)
        {
            case "Linear":
                PlayerController.Instance.player_Data.shoot_path = ShootPath.linear;
                break;
            case "Beam":
                PlayerController.Instance.player_Data.shoot_path = ShootPath.beam;
                break;
            case "Spiral":
                PlayerController.Instance.player_Data.shoot_path = ShootPath.spiral;
                break;
            case "Curve":
                PlayerController.Instance.player_Data.shoot_path = ShootPath.curve;
                break;
            case "Random":
                PlayerController.Instance.player_Data.shoot_path = ShootPath.random;
                break;
        }
    }

    public void SetBulletNum()
    {
        switch (shoot_path.options[shoot_path.value].text)
        {
            case "One":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.one;
                break;
            case "Multi_2":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.multi_2;
                break;
            case "Multi_3":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.multi_3;
                break;
            case "Multi_4":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.multi_4;
                break;
            case "Multi_8":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.multi_8;
                break;
            case "SequentialShot":
                PlayerController.Instance.player_Data.bullet_num = BulletNum.sequential_shot;
                break;
        }

    }

    public void ChangedValueReset()
    {
        PlayerController.Instance.player_Data.changed.max_health = 0;
        PlayerController.Instance.player_Data.changed.max_stamina = 0;
        PlayerController.Instance.player_Data.changed.move_speed = 0;
        PlayerController.Instance.player_Data.changed.shoot_speed = 0;
        PlayerController.Instance.player_Data.changed.attack_power = 0;
    }

    public void SetValue(float max_hp, float max_st, float att, float m_spd, float s_spd)
    {
        PlayerController.Instance.player_Data.max_health += max_hp;
        PlayerController.Instance.player_Data.max_stamina += max_st;
        PlayerController.Instance.player_Data.attack_power += att;
        PlayerController.Instance.player_Data.move_speed += m_spd;
        PlayerController.Instance.player_Data.shoot_speed += s_spd;

        PlayerController.Instance.player_Data.SaveState();
    }

    IEnumerator Die()
    {
        controller.pause = true;
        //~~대가 죽었다.
        game_ui.DOFade(0, 0.5f);

        game_over.DOFade(1, 0.3f);
        die_msg.DOFade(1, 0.3f);
        die_msg.text = name + "가문 " + PlayerController.Instance.player_Data.generation.ToString() + "대가 죽었다.";

        yield return new WaitForSeconds(0.3f);

        //플레이어 Die 애니메이션
        player_anim.CrossFade("die", 0.3f);

        yield return new WaitForSeconds(0.5f);

        die_eff.Play();

        yield return new WaitForSeconds(1.0f);



        //적 없애기
        if (maps.room_num == Define.ROOM_NUM - 1)
        {   //보스방
            maps.boss_room.KillBoss(true);
            maps.boss_room.npc[maps.boss_room.npc_num].SetActive(false);
            maps.boss_room.prison.SetActive(false);
        }
        else
            maps.set_obj[maps.room_num].KillEnemy(true);

        //플레이어 멈추기
        PlayerController.Instance.player_Animate.SendMessage("Freeze");

        yield return new WaitForSeconds(1.0f);

        die_msg.DOFade(0, 0.3f);
        game_over.DOFade(0, 0.3f);

        //팝업
        PlayerController.Instance.player_Revive.Setting();
        PlayerController.Instance.player_Revive.StartCoroutine("ShowWindow", true);
    }
}