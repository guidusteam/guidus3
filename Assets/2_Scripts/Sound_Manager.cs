﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Sound_Manager : Singleton<Sound_Manager> {

    public AudioClip intro, caravan, dungeon, boss;

    public AudioClip hit, shoot;

    public AudioSource bgm_source;
    public AudioSource eff_source;
    
    IEnumerator BGMPlay(string name)
    {
        bgm_source.DOFade(0.5f, 1f);
        yield return new WaitForSeconds(1.0f);

        switch (name)
        {
            case "intro":
                bgm_source.clip = intro;           
                break;
            case "caravan":
                bgm_source.clip = caravan;
                break;
            case "dungeon":
                bgm_source.clip = dungeon;
                break;
            case "boss":
                bgm_source.clip = boss;
                break;
        }

        bgm_source.DOFade(0.9f, 2f);
        bgm_source.Play();
        bgm_source.loop = true;
    }

    public void EffPlay(string name)
    {
        switch (name)
        {
            case "hit":
                eff_source.clip = hit;
                break;
            case "shoot":
                eff_source.clip = shoot;
                break;
        }
        eff_source.Play();
    }
}
