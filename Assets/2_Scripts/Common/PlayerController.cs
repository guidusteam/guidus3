﻿using UnityEngine;
using System.Collections;

public class PlayerController : Singleton<PlayerController> {

    public GameObject player;

    [System.NonSerialized]
    public Transform player_Transform;
    [System.NonSerialized]
    public Player_Animate player_Animate;
    [System.NonSerialized]
    public Player_Collision player_Collision;
    [System.NonSerialized]
    public Player_Revive player_Revive;
    [System.NonSerialized]
    public Player_Shoot player_Shoot;
    [System.NonSerialized]
    public Player_State player_State;
    [System.NonSerialized]
    public Player_Data player_Data;

    void Awake()
    {
        player_Transform = player.transform;
        player_Animate = player.GetComponent<Player_Animate>();
        player_Collision = player.GetComponent<Player_Collision>();
        player_Revive = player.GetComponent<Player_Revive>();
        player_Shoot = player.GetComponentInChildren<Player_Shoot>();
        player_State = player.GetComponent<Player_State>();
        player_Data = player.GetComponent<Player_Data>();
    }
}
