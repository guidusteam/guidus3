﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class Manager : Singleton<Manager> {
    
    void Start()
    {
        //ads = Player.GetComponent<AudioSource>();
        //background_sound = GetComponent<AudioSource>();

        if (Advertisement.isSupported)
        {
            Advertisement.allowPrecache = true;
#if UNITY_ANDROID
            Advertisement.Initialize("114276", false);	//(appID, TestMode)
#elif UNITY_IOS
			;
#endif
        }
        else
        {
            Debug.Log("Platform not supported");
        }
  
    }
}
