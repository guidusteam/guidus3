﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Intro : MonoBehaviour {
    
    WWW_Class www;

    public GameObject intro_ui;
    public GameObject setting_ui;
    public Text error_msg;
    public InputField input;
    public Button ok_button;

    CanvasGroup intro_cvs;
    public Text name, floor, hp, stm, att_power, move_spd, att_spd;
    bool start = false;
    public bool setting = false;

    void Start()
    {
        intro_cvs = intro_ui.GetComponent<CanvasGroup>();
        www = GetComponent<WWW_Class>();

        //www.StartCoroutine("Setting");
        PlayerController.Instance.player_Data.LoadState();  
        ShowIntro();
    }

    public void Setting()
    {
        string name = input.text;
        if(input.text.Length > 6 || input.text.Length < 2)
        {
            StartCoroutine("ShowErrMsg");
        }
        else
        {
            input.enabled = false;
            ok_button.enabled = false;
            www.StartCoroutine("SendFName", input.text);
        }
    }

    IEnumerator ShowErrMsg()
    {
        error_msg.enabled = true;
        error_msg.DOFade(1, 0.5f);
        yield return new WaitForSeconds(2.0f);
        error_msg.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        error_msg.enabled = false;
    }

    public void ShowIntro()
    {
        setting_ui.SetActive(false);
        name.text = PlayerController.Instance.player_State.name + "가문 " + PlayerController.Instance.player_Data.generation.ToString() + "대손";
        floor.text = "현재 지하 " + PlayerController.Instance.player_Data.floor.ToString() + "층";

        hp.text = PlayerController.Instance.player_Data.health.ToString();
        stm.text = PlayerController.Instance.player_Data.stamina.ToString();
        att_power.text = PlayerController.Instance.player_Data.attack_power.ToString();
        move_spd.text = ((1 - PlayerController.Instance.player_Data.move_speed) * 100).ToString();
        att_spd.text = ((1 - PlayerController.Instance.player_Data.shoot_speed) * 100).ToString();

        intro_cvs.DOFade(1, 0.5f);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !start )// && setting)
        {
            //사라짐
            start = true;
            intro_cvs.DOFade(0, 1.0f).OnComplete(() => GotoCaravan());
        }
    }

   void GotoCaravan()
    {
        //caravan 세팅
        Application.LoadLevel("Caravan");
    }
}
