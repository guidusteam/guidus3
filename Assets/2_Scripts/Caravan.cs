﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Caravan : MonoBehaviour {
    
    public GameObject caravan_ui;
    public GameObject caravan_map;
    public Text floor;
    public Text Gold;
    //Fade fade;
    CanvasGroup floor_cvs;
    Controller controller;
    Camera main_camera;
    Ray ray;
    RaycastHit hit;
    
    public NPC[] npc;

    void Start () {
        caravan_ui.transform.DOScale(2.0f, 0.0f);
        floor_cvs = floor.GetComponent<CanvasGroup>();
        controller = GetComponent<Controller>();

        main_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        main_camera.transform.position = new Vector3(23, 50, -46);
        PlayerController.Instance.player_Data.LoadState();
        PlayerController.Instance.player_Animate.in_caravan = true;
        this.enabled = false;

        Setting();
    }

    void Setting()
    {
        Sound_Manager.Instance.StartCoroutine("BGMPlay", "caravan");
        //caravan UI 활성화
        caravan_ui.SetActive(true);
        caravan_ui.transform.DOScale(1.0f, 0.5f);
        //층
        floor.text = PlayerController.Instance.player_Data.floor + "층";
        floor_cvs.alpha = 0;

        //코인
        Gold.text = "Gold : " + PlayerController.Instance.player_Data.gold;


        //구출한 npc 표시
        for (int i = 0; i < npc.Length; ++i)
        {
            if (Define.NPC_ACTIVE[i])
            {
                npc[i].gameObject.SetActive(true);
                npc[i].StartCoroutine("ShowMent");
                //GridManager.instance.CalculateObj(npc[i].transform.position, ObjType.NPC);
            }
        }

        //GridManager.instance.CalculateFlr(Define.door_pos[(int)Dir.down], FlrType.exit);
        //GridManager.instance.CalculateFlr(Define.door_pos[Define.door_count + (int)Dir.down], FlrType.exit);


        //플레이어 애니메이션, 이동 가능하게
        //PlayerController.Instance.player_Animate.SendMessage("UnFreeze");
    }

    public IEnumerator GotoCaravan()
    {
        print("Caravan");
        // 플레이어 애니메이션, 이동 가능하게
        PlayerController.Instance.player_Animate.in_caravan = true;
        PlayerController.Instance.player_Animate.SendMessage("UnFreeze");

        Sound_Manager.Instance.StartCoroutine("BGMPlay", "caravan");
        Fade.Instance.FadeOut(0);

        yield return new WaitForSeconds(0.5f);

        //층
        floor.text = PlayerController.Instance.player_Data.floor + "층";
        floor_cvs.alpha = 0;

        //코인
        Gold.text = "Coin : " + PlayerController.Instance.player_Data.gold;

        //npc
        for (int i = 0; i < npc.Length; ++i)
        {
            if (Define.NPC_ACTIVE[i])
            {
                npc[i].gameObject.SetActive(true);
                npc[i].StartCoroutine("ShowMent");
                GridManager.instance.CalculateObj(npc[i].transform.position, ObjType.NPC);
            }
        }
        
        Fade.Instance.FadeIn();

        yield return new WaitForSeconds(0.5f);

        controller.pause = false;
    }


    public IEnumerator StartGame()
    {
        controller.pause = true;
        //fade out
        Fade.Instance.FadeOut(1.0f);
        Fade.Instance.FadeIn_group(floor_cvs);  //층

        Sound_Manager.Instance.StartCoroutine("BGMPlay", "dungeon");

        yield return new WaitForSeconds(1.0f);

        //게임 진행
        //PlayerController.Instance.player_Animate.Setting(true);
        PlayerController.Instance.player_Animate.in_caravan = false;

        //fade in
        Fade.Instance.FadeOut_group(floor_cvs);

        this.enabled = false;

        yield return new WaitForSeconds(0.5f);

        controller.pause = false;
        Application.LoadLevel("Game(0~10)");
    }
}
