﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Fade : Singleton<Fade> {

    public Image fade_scr;
    public Image indicator;

    //밝아짐
    public void FadeIn(float fade_time = 0.5f)
    {
        fade_scr.enabled = true;
        fade_scr.color = new Vector4(0, 0, 0, 1);
        fade_scr.DOFade(0, fade_time);
    }

    //어둡
    public void FadeOut(float fade_time = 0.5f)
    {
        fade_scr.enabled = true;
        fade_scr.color = new Vector4(0, 0, 0, 0);
        fade_scr.DOFade(1, fade_time);
    }

   // public void FadeInOut_group(CanvasGroup group)
   // {
   //     group.DOFade(1, fade_time).OnComplete(() =>
   //      FadeOut_group(group));
   // }

    public void FadeIn_group(CanvasGroup group, float fade_time = 0.5f)
    {
        group.DOFade(1, fade_time);
    }

    public void FadeOut_group(CanvasGroup group, float fade_time = 0.5f)
    {
        group.DOFade(0, fade_time);
    }

    IEnumerator FadeOut(Color obj_color)
    {
        for (float i = 1f; i >= 0; i -= 0.1f)
        {
            Color set_color = new Vector4(1, 1, 1, i);
            obj_color = set_color;
            yield return 0;
        }
    }

    IEnumerator FadeIn(Color obj_color)
    {
        print("FI");
        for (float i = 0f; i <= 1; i += 0.1f)
        {
            Color set_color = new Vector4(1, 1, 1, i);
            obj_color = set_color;
            yield return 0;
        }
    }

    IEnumerator ShowIndicator(bool first = true)
    {
        if(first)indicator.DOFade(1, 0.5f);

        for (int i = 0; i < 8; ++i)
        {
            indicator.rectTransform.DORotate(new Vector3(0, 0, 45 * i), 0.3f);
            yield return new WaitForSeconds(0.3f);
        }
        StartCoroutine("ShowIndicator", false);
    }

    public void HideIndicator()
    {
        StopCoroutine("RotateIndicator");
        indicator.DOFade(0, 0.5f);
    }

}

