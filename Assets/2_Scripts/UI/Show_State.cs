﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Show_State : MonoBehaviour {

    public Text attack;
    public Text move_v;
    public Text shoot_v;
    public Text floor;
    public Slider hp_bar;
    public Slider st_bar;
	
	void Start () {
        UpdateAbility();
        UpdateFloor();
	}
	
	void UpdateFloor()
    {
        floor.text = "-지하 " + PlayerController.Instance.player_Data.floor.ToString() + "층-";
    }

    IEnumerator ShowHeadText( TextMesh text )
    {
        text.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        text.gameObject.SetActive(false);
    }

	void UpdateAbility () {
        attack.text = (PlayerController.Instance.player_Data.attack_power+ PlayerController.Instance.player_Data.changed.attack_power).ToString();
        move_v.text = ((1 - PlayerController.Instance.player_Data.move_speed- PlayerController.Instance.player_Data.changed.move_speed) * 100).ToString();
        shoot_v.text =((1 - PlayerController.Instance.player_Data.shoot_speed- PlayerController.Instance.player_Data.changed.shoot_speed) * 100).ToString();
    }

    public void Update_HP()
    {
        hp_bar.value = PlayerController.Instance.player_Data.health / (PlayerController.Instance.player_Data.max_health + PlayerController.Instance.player_Data.changed.max_health);
    }

    public void Update_ST()
    {
        st_bar.value = PlayerController.Instance.player_Data.stamina / (PlayerController.Instance.player_Data.max_stamina + PlayerController.Instance.player_Data.changed.max_stamina);
    }
}
