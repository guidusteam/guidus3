﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Controller : MonoBehaviour {
    
    Animation player_anim;
    Vector3 att_dir, move_dir;
    bool is_shooting = false, is_moving = false;
    public bool pause = false;

    void Start () {
        player_anim = PlayerController.Instance.player_Animate.player_anim;
	}

    public void StartAttack()
    {
        if (pause)
        {
            QuitAttack();
            return;
        }
        is_shooting = true;

        att_dir = new Vector3(ETCInput.GetAxis("Right_X"), 0, ETCInput.GetAxis("Right_Y"));

        Quaternion look_rot = Quaternion.LookRotation(att_dir);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;

        if (!is_moving)
            player_anim.CrossFade("attack", 0.1f);
        else
        {
            if (Vector3.Angle(att_dir, move_dir) < 90)
            {
                player_anim.CrossFade("moveattack",0.3f);
            }
            else
            {
                player_anim.CrossFade("backmoveattack", 0.3f);
            }
        }
        PlayerController.Instance.player_Animate.Rotate(dest_rot, true);
        PlayerController.Instance.player_Shoot.ShootBullet(att_dir.normalized);
    }

    public void QuitAttack()
    {
        player_anim.CrossFade("idle", 0.3f);
        is_shooting = false;
    }
   
    public void StartMove()
    {
        if (pause)
        {
            QuitMove();
            return;
        }
        is_moving = true;

        move_dir = new Vector3(ETCInput.GetAxis("Left_X"), 0, ETCInput.GetAxis("Left_Y"));
        
        Quaternion look_rot = Quaternion.LookRotation(move_dir);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;

        if (!is_shooting)
        {            
            player_anim.CrossFade("run", 0.3f);
            PlayerController.Instance.player_Animate.Rotate(dest_rot, false);
        }
        else    //걸으면서 쏘는 동작
        {
            if (Vector3.Angle(att_dir, move_dir) < 90)
            {
                player_anim.CrossFade("moveattack", 0.3f);
            }
            else
            {
                player_anim.CrossFade("backmoveattack", 0.3f);
            }
        }
        PlayerController.Instance.player_Animate.Move(move_dir.normalized);
    }

    public void QuitMove()
    {
        player_anim.CrossFade("idle", 0.3f);

        PlayerController.Instance.player_Transform.DOKill();
        PlayerController.Instance.player_Animate.camera_trans.DOKill();
        is_moving = false;
    }
}
