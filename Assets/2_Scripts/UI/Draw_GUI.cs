﻿using UnityEngine;
using System.Collections;

public class Draw_GUI : MonoBehaviour {
    
    public string item_name, item_detail;

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        GUI.Label(new Rect(10, 10, 300, 50), item_name);
        GUI.Label(new Rect(10, 50, 300, 50), item_detail);
        
        GUI.Label(new Rect(500, 100, 300, 50), "속도" + PlayerController.Instance.player_Data.move_speed.ToString());
    }
}
