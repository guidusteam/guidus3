﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour {

    Animation gre_anim;
    public ParticleSystem exp_eff;
    
    float anim_time;

    void Awake()
    {
        gre_anim = GetComponent<Animation>();
        anim_time = gre_anim.GetClip("Open").length;
    }

    IEnumerator Explode(Vector3 pos)
    {
        gre_anim.Play("Open");

        yield return new WaitForSeconds(anim_time);

        exp_eff.gameObject.SetActive(true);
        exp_eff.Play();

        yield return new WaitForSeconds(exp_eff.duration);
        
        gre_anim.Play("Close");
        exp_eff.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
