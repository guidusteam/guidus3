﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Swirl : MonoBehaviour {

    public GameObject background, top, open_light;
    Transform back_trans, top_trans;

    void Start()
    {
        back_trans = background.transform;
        top_trans = top.transform;
    }

    void Update()
    {
        back_trans.Rotate(new Vector3(0, 0, 1), 2);
        top_trans.Rotate(new Vector3(0, 0, 1), 5);
    }

    public void OpenEffect()
    {
        open_light.SetActive(true);
    }

    public void Hide()
    {
        open_light.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
