﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Door : MonoBehaviour {
    public bool[] isDoor;
    public bool[] isOpened;
    public GameObject[] doors;
    public Swirl[] swirl;
    
    public Maps maps;
    public bool enemy_die = false;

    public Camera main_camera;
    Ray ray;
    RaycastHit hit;



    public void EnemyDie()
    {
        enemy_die = true;

        for (int i = 0; i < doors.Length; ++i)
            if (isDoor[i])
            {
                isOpened[i] = true;
                OpenDoor(i);
            }
    }

    public void Reset()
    {
        HideDoor();
        for (int i = 0; i < doors.Length; ++i)
        {
            isDoor[i] = false;
            isOpened[i] = false;

            //GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[i]);
            //GridManager.instance.SendMessage("ResetFlrNode", Define.door_pos[i + Define.door_count]);
        }
    }

    public void HideDoor()
    {
        for (int i = 0; i < doors.Length; ++i)
        {
            doors[i].SetActive(true);
            swirl[i].Hide();
        }
    }

    public void OpenDoor(int num)
    {
        //open_img[num].SetActive(true);
        swirl[num].OpenEffect();
    }

    public void DrawDoor()
    {
        for (int i = 0; i < doors.Length; ++i)
        {
            if (isDoor[i])
            {

                if (isOpened[i]) OpenDoor(i);
                doors[i].SetActive(false);
                swirl[i].gameObject.SetActive(true);
                //GridManager.instance.CalculateFlr(Define.door_pos[i], FlrType.door);
                //GridManager.instance.CalculateFlr(Define.door_pos[Define.door_count + i], FlrType.door);
            }
            else
                doors[i].SetActive(true);
        }
    }
}
