﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Boss_Room : MonoBehaviour {

    public GameObject stair;
    public GameObject stair_wall;
    public Door door;
    public GameObject[] boss;
    public GameObject[] npc;
    public Maps maps;

    Vector3[] set_pos;
    public GameObject prison;
    public Set_Object set_obj;

    int boss_num;
    public int npc_num;
    bool once = true;
    public Mini_Map minimaps;

    public Camera main_camera;

    Ray ray;
    RaycastHit hit;

    void Awake()
    {
        boss_num = boss.Length;

        set_pos = new Vector3[boss.Length];
        for (int i = 0; i < set_pos.Length; ++i)
        {
            set_pos[i] = boss[i].transform.position;
        }
    }

    void Update()
    {
        GetEvent();
    }
   
    void GetEvent()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.CompareTag("Stair") && 
                    Vector3.Distance(stair.transform.position, PlayerController.Instance.player_Transform.position) <= 10)
                {
                    stair.SetActive(false);

                    this.gameObject.SetActive(false);
                    npc[npc_num].SetActive(false);

                    PlayerController.Instance.player_Animate.Setting(true);
                    PlayerController.Instance.player_Data.floor--;
                    PlayerController.Instance.player_Data.SaveState();
                    
                    stair_wall.SetActive(true);
                    maps.SendMessage("Reset");
                    maps.SendMessage("Setting");

                    //Application.LoadLevel("Caravan");
                }
            }
        }           
    }

    public void CallBoss(){
        Sound_Manager.Instance.StartCoroutine("BGMPlay", "boss");
        for (int i = 0; i < boss.Length; ++i)
        {
            boss[i].SetActive(true);
            boss[i].transform.position = set_pos[i];
            boss[i].SendMessage("SetTarget");
        }
        boss_num = boss.Length;
        once = true;


        //npc
        for (int i = 0; i < Define.NPC_FLOOR.Length; ++i) {
            //npc 구출해야하는 층이고 npc가 아직 활성화되지 않았을 때
            if (PlayerController.Instance.player_Data.floor == Define.NPC_FLOOR[i] &&Define.NPC_ACTIVE[i] == false)
            {
                npc_num = i;
                prison.SetActive(true);
                prison.transform.DOMoveY(0, 0);//제자리
                npc[npc_num].SetActive(true);
            }
       }
    }

    public bool CheckBoss(int died_num = 0)
    {
        boss_num += died_num;
        if(boss_num <= 0)
        {
            BossDied();
            return true;
        }
        return false;
    }

    public void KillBoss(bool player_die)
    {
        for (int i = 0; i < boss.Length; ++i)
          boss[i].SetActive(false);

        if (!player_die) CheckBoss(-boss.Length);
        
        set_obj.KillEnemy(player_die);
    }

    public void BossDied()
    {
        if (once)
        {
            stair.SetActive(true);
            //계단구멍
            stair_wall.SetActive(false);
            door.SendMessage("EnemyDie");
            once = false;

            set_obj.KillEnemy(false);
            prison.transform.DOMoveY(-4, 0.5f);
            //구출
            Define.NPC_ACTIVE[npc_num] = true;
        }
    }
}
