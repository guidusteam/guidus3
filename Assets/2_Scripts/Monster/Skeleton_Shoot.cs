﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Skeleton_Shoot : MonoBehaviour {

    int arrow_damage = 5;
    float arrow_speed = 1.5f;
    int arrow_dist = 80;
    
    public GameObject arrow;
    Transform arrow_trans;
    public Transform shoot_point;
    Skeleton_Animate skel_animate;

	void Start () {
        skel_animate = GetComponent<Skeleton_Animate>();
        arrow_trans = arrow.transform;
	}
	
    IEnumerator Shoot(Vector3 dest)
    {
        yield return new WaitForSeconds(0.7f);
        arrow.SetActive(true);

        //나->플레이어
        dest.Normalize();
        
        arrow_trans.position = shoot_point.position;
        arrow.SetActive(true);
        arrow_trans.DOMove(shoot_point.position + dest * arrow_dist, arrow_speed);

        yield return new WaitForSeconds(arrow_speed);

        arrow.SetActive(false);
        skel_animate.SendMessage("FindPath");
    }
}
