﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Skeleton_Animate : MonoBehaviour {

    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    
    Set_Object set_obj;
    Item_Manager item_mng;
    Animation skel_anim;
    public Skeleton_Shoot skel_shoot;
    Transform skel_trans;

    public string my_type;
    int move_count = 0;
    float move_speed = 0.5f;
    public bool defence = false;
    int attack_power = 7;

    // Use this for initialization
    void SetTarget()
    {
        skel_anim = GetComponent<Animation>();
        skel_trans = transform;
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();

        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
    }


    void FindPath()
    {
        if (move_count >= 3)
        {    //3칸 움직이고 한번 휴식
            skel_trans.DOKill();
            if (my_type == "sword_skeleton")
                StartCoroutine("Break");
            else if (my_type == "arrow_skeleton")
            {
                move_count = 0;
                pathArray.Clear();
                skel_anim.CrossFade("attack");

                Vector3 dest = PlayerController.Instance.player_Transform.position - skel_trans.position;
                Quaternion look_rot = Quaternion.LookRotation(dest);
                Vector3 dest_rot = look_rot.eulerAngles;

                skel_shoot.StartCoroutine("Shoot", dest);             
            }
            else if(my_type == "shield_skeleton")
            {
                if(Vector3.Distance(PlayerController.Instance.player_Transform.position, skel_trans.position) <= 10)
                {
                    //방패
                    StartCoroutine("Defence");
                }
                else
                {
                    //휴식
                    StartCoroutine("Break");
                }
            }
            return;
        }

        startPos = skel_trans;
        endPos = PlayerController.Instance.player_Transform;
        endPos.position = new Vector3(PlayerController.Instance.player_Transform.position.x, 0, PlayerController.Instance.player_Transform.position.z);

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if(my_type == "sword_skeleton" || my_type == "shield_skeleton")
        {
            if (pathArray.Count <= 2)
            {
                StartCoroutine("Attack");
            }
            else Move();
        }
        else if(my_type == "arrow_skeleton")
        {
            if (pathArray.Count <= 4)
            {
                move_count = 0;
                pathArray.Clear();
                skel_anim.CrossFade("attack");

                //회전
                Vector3 dest = PlayerController.Instance.player_Transform.position - skel_trans.position;
                Quaternion look_rot = Quaternion.LookRotation(dest);
                Vector3 dest_rot = look_rot.eulerAngles;


                skel_trans.DORotate(dest_rot, 0.3f);

                skel_shoot.StartCoroutine("Shoot", dest);
            }
            else Move();
        }

    }

    IEnumerator Break()
    {       
        skel_anim.CrossFade("idle");
        pathArray.Clear();

        yield return new WaitForSeconds(3.0f);

        move_count = 0;
        FindPath();
    }

    void Move()
    {
        //현재 move동작이 아닐때만 새로 재생
        if (!skel_anim.IsPlaying("run"))
            skel_anim.CrossFade("run");
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", skel_trans.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        skel_trans.DOMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - skel_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        skel_trans.DORotate(dest_rot, move_speed);
        move_count++;
    }

    IEnumerator Attack()
    {
        if (!skel_anim.IsPlaying("attack"))
            skel_anim.CrossFade("attack");
        move_count = 0;

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - skel_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        skel_trans.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.5f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - skel_trans.position).sqrMagnitude < Define.ATTACK_BOUND)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        skel_anim.CrossFade("die");
        yield return new WaitForSeconds(0.9f);

        GridManager.instance.SendMessage("ResetObjNode", skel_trans.position);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(skel_trans.position);
        this.gameObject.SetActive(false);
    }

    IEnumerator Defence()
    {
        defence = true;
        skel_anim.CrossFade("defence");
        pathArray.Clear();

        yield return new WaitForSeconds(3.0f);

        defence = false;
        move_count = 0;
        FindPath();
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            for (int i = 0; i < pathArray.Count; ++i)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(((Node)pathArray[i]).position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}
