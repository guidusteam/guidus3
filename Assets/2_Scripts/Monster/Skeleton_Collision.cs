﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Skeleton_Collision : MonoBehaviour {

    float health;
    Skeleton_Animate skell_animate;
    public Renderer render;
    float damage;

    void Start()
    {
        skell_animate = GetComponent<Skeleton_Animate>();
        if (skell_animate.my_type == "sword_skeleton")
            health = Define.SKEL_HP;
        else
            health = Define.SKELD_HP;
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            //방어중일때 무시
            if (skell_animate.defence) return;

            damage = PlayerController.Instance.player_Data.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        if (skell_animate.my_type == "sword_skeleton")
            health = Define.SKEL_HP;
        else
            health = Define.SKELD_HP;
        this.transform.DOKill();
        skell_animate.StartCoroutine("Die");
    }
}
