﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Orc_Animate : MonoBehaviour
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    Set_Object set_obj;
    Item_Manager item_mng;
    Animator orc_action;
    Transform orc_trans;

    float move_speed = 0.5f;

    int attack_power = 8;

    // Use this for initialization

    void SetTarget()
    {
        orc_action = this.gameObject.GetComponent<Animator>();
        orc_trans = transform;
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();

    }


    void FindPath()
    {
        startPos = orc_trans;
        endPos = PlayerController.Instance.player_Transform;
        endPos.position = new Vector3(PlayerController.Instance.player_Transform.position.x, 0, PlayerController.Instance.player_Transform.position.z);

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if (pathArray.Count <= 2) StartCoroutine("Attack");
        else Move();


    }

    void Move()
    {
        orc_action.CrossFade("Run", 0.2f);
        orc_action.SetBool("attack", false);
        orc_action.SetBool("run", true);
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", orc_trans.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        orc_trans.DOMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());// StartCoroutine("Break"));

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - orc_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        orc_trans.DORotate(dest_rot, move_speed);
    }

    IEnumerator Attack()
    {
        orc_action.SetBool("run", false);
        orc_action.SetBool("attack", true);
        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - orc_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        orc_trans.DORotate(dest_rot, 0.2f);
        yield return new WaitForSeconds(0.5f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - orc_trans.position).sqrMagnitude <= Define.ATTACK_BOUND)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        orc_action.CrossFade("Die", 0.3f);
        yield return new WaitForSeconds(0.6f);

        GridManager.instance.SendMessage("ResetObjNode", orc_trans.position);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(orc_trans.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            for (int i = 0; i < pathArray.Count; ++i)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(((Node)pathArray[i]).position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}