﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class SkeletonK_Collision : MonoBehaviour {

    float health = Define.SKELK_HP;
    SkeletonK_Animate skellK_animate;
    public Renderer render;
    float damage;

    void Start()
    {
        skellK_animate = GetComponent<SkeletonK_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            //방어중일때 무시
            if (skellK_animate.defence) return;

            damage = PlayerController.Instance.player_Data.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = Define.SKELK_HP;
        this.transform.DOKill();
        skellK_animate.StartCoroutine("Die");
    }
}
