﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Slime_Collision : MonoBehaviour {

    float health = Define.SLIME_HP;
    public Renderer render;
    Slime_Animate slime_animate;
    float damage;

    void Start()
    {
        slime_animate = GetComponent<Slime_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            //================총알타입===============

            //독
            if (PlayerController.Instance.player_Data.bullet_type == BulletType.poison)
            {
                render.material.DOColor(Color.green, 0.5f);
                StartCoroutine("Poisoning");
            }

            //얼음
            else if (PlayerController.Instance.player_Data.bullet_type == BulletType.freeze)
            {
                render.material.DOColor(Color.cyan, 0.5f);
                slime_animate.StopAllCoroutines();
                slime_animate.StartCoroutine("Freeze");
            }

            damage = PlayerController.Instance.player_Data.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    IEnumerator Poisoning()
    {
        health -= Define.POSION_DAMAGE;
        if (this.health <= 0) Kill();
        yield return new WaitForSeconds(2.0f);

        StartCoroutine("Poisoning");
    }

    public void Kill()
    {
        health = Define.SLIME_HP;
        this.transform.DOKill();
        slime_animate.StartCoroutine("Die");
    }
}