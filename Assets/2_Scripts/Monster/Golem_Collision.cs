﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Golem_Collision : MonoBehaviour {
    float health;
    Golem_Animate golem_animate;
    public Renderer render;
    float damage;

    void Start()
    {
        health = Define.GOLEM_HP;
        golem_animate = GetComponent<Golem_Animate>();
        damage = PlayerController.Instance.player_Data.attack_power;
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            //================총알타입===============
            
            //독
            if (PlayerController.Instance.player_Data.bullet_type == BulletType.poison)
            {
                render.material.DOColor(Color.green, 0.5f);
            }

            //얼음
            else if(PlayerController.Instance.player_Data.bullet_type == BulletType.freeze)
            {
                render.material.DOColor(Color.cyan, 0.5f);
            }



            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = Define.GOLEM_HP;
        this.transform.DOKill();
        golem_animate.StartCoroutine("Die");
    }
}
