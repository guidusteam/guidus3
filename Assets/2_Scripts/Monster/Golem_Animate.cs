﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Golem_Animate : MonoBehaviour
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    
    Animation golem_anim;
    Boss_Room boss_room;
    Transform golem_trans;
    float move_speed = 0.7f;
    int attack_power = 10;

    // Use this for initialization

    void SetTarget()
    {
        golem_anim = this.gameObject.GetComponent<Animation>();
        golem_trans = transform;
        boss_room = GetComponentInParent<Boss_Room>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
    }


    void FindPath()
    {
        startPos = golem_trans;
        endPos = PlayerController.Instance.player_Transform;

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);


        if (pathArray.Count <= 3)
        {
            if (Random.Range(0, 100) < 50)
                StartCoroutine("Attack");
            else
                StartCoroutine("Attack2");
        }
        else if (pathArray.Count > 3) Move();
    }

    void Move()
    {
        if (!golem_anim.IsPlaying("run"))
            golem_anim.CrossFade("run", 0.3f);
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", golem_trans.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        golem_trans.DOLocalMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());

       //회전값
       Quaternion look_rot = Quaternion.LookRotation(node.position - golem_trans.localPosition);
       Vector3 dest_rot = look_rot.eulerAngles;
        golem_trans.DORotate(dest_rot, move_speed);
    }

    IEnumerator Attack()
    {
        if (!golem_anim.IsPlaying("attack0"))
            golem_anim.CrossFade("attack0", 0.3f);

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - golem_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        golem_trans.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(1.0f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - golem_trans.position).sqrMagnitude < (Define.ATTACK_BOUND + 1))
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.8f);

        StartCoroutine("Break");
    }

    IEnumerator Attack2()
    {
        int num = Random.Range(1, 3);   //1 2
        if (!golem_anim.IsPlaying("attack" + num))
            golem_anim.CrossFade("attack" + num, 0.3f);

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - golem_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        golem_trans.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.7f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - golem_trans.position).sqrMagnitude < Define.ATTACK_BOUND+1)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.6f);

        StartCoroutine("Break");
    }

    IEnumerator Break()
    {
        golem_anim.CrossFade("idle", 1.0f);

        yield return new WaitForSeconds(2.0f);

        FindPath();
    }

    IEnumerator Die()
    {
        golem_anim.CrossFade("die", 0.3f);
        yield return new WaitForSeconds(1.5f);

        GridManager.instance.SendMessage("ResetObjNode", golem_trans);

        boss_room.CheckBoss(-1);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            for(int i = 0; i < pathArray.Count; ++i)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(((Node)pathArray[i]).position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}