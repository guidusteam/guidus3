﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Worm_Collision : MonoBehaviour {

    float health = Define.WORM_HP;
    Worm_Animate worm_animate;
    float damage;
    public Renderer render;

    void Start()
    {
        worm_animate = GetComponent<Worm_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            damage = PlayerController.Instance.player_Data.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = Define.WORM_HP;
        this.transform.DOKill();
        worm_animate.StartCoroutine("Die");
    }

}
