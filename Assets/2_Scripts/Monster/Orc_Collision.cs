﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Orc_Collision : MonoBehaviour
{
    public Renderer render;
    float health = Define.ORC_HP;
    Orc_Animate orc_animate;

    float damage;

    void Start()
    {
        orc_animate = GetComponent<Orc_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet")|| other.CompareTag("Explosion"))
        {
            damage = PlayerController.Instance.player_Data.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = Define.ORC_HP;
        this.transform.DOKill();
        orc_animate.StartCoroutine("Die");
    }
}
