﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Slime_Animate : MonoBehaviour 
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    public Renderer render;

    Transform slime_trans;
    Animator slime_action;
    Set_Object set_obj;
    Item_Manager item_mng;
    Color own_color;

    float move_speed = 0.4f;
    int attack_power = 3;

	// Use this for initialization
    void SetTarget()
    {
        slime_action = this.gameObject.GetComponent<Animator>();
        slime_trans = transform;
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        own_color = render.material.color;
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();      
    }


    void FindPath()
    {
        startPos = slime_trans;
        endPos = PlayerController.Instance.player_Transform;
        endPos.position = new Vector3(PlayerController.Instance.player_Transform.position.x, 0, PlayerController.Instance.player_Transform.position.z);
        
        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if (pathArray.Count == 2) StartCoroutine("Attack");
        else if (pathArray.Count < 2) StartCoroutine("Break");
        else Move();
            

    }

    void Move()
    {
        slime_action.SetBool("attack", false);
        slime_action.SetBool("move", true);
        Node prev = (Node)pathArray[0];
        Node node = (Node)pathArray[1];
        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", prev.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - slime_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        slime_trans.DORotate(dest_rot, move_speed);

        slime_trans.DOMove(node.position, move_speed).SetEase(Ease.InOutCubic).OnComplete(() => StartCoroutine("Break"));  
        
    }

    IEnumerator Break()
    {
        slime_action.SetBool("move", false);

        yield return new WaitForSeconds(0.8f);

        FindPath();
    }

    IEnumerator Freeze()
    {
        pathArray.Clear();
        slime_action.SetBool("move", false);
        slime_action.SetBool("attack", false);
        slime_action.Stop();

        yield return new WaitForSeconds(1.5f);

        render.material.DOColor(own_color, 0.5f);
        slime_action.Play("idle");
        FindPath();
    }

    IEnumerator Attack()
    {
        slime_action.SetBool("move", false);
        slime_action.SetBool("attack", true);

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - slime_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        slime_trans.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.6f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - slime_trans.position).sqrMagnitude < Define.ATTACK_BOUND)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.4f);

        FindPath();
    }

    IEnumerator Die()
    {
        slime_action.SetBool("die", true);
        yield return new WaitForSeconds(0.6f);

        GridManager.instance.SendMessage("ResetObjNode", slime_trans.position);

        slime_action.SetBool("die", false);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(slime_trans.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}