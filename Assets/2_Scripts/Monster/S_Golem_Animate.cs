﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class S_Golem_Animate : MonoBehaviour
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    Item_Manager item_mng;
    Set_Object set_obj;
    Animator golem_action;
    Transform golem_trans;


    float move_speed = 0.75f;
    int attack_power = 5;


    // Use this for initialization
    void SetTarget()
    {
        golem_action = this.gameObject.GetComponent<Animator>();
        golem_trans = transform;
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
    }


    void FindPath()
    {
        startPos = golem_trans;
        endPos = PlayerController.Instance.player_Transform;

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);


        if (pathArray.Count <= 2) StartCoroutine("Attack");
        else if (pathArray.Count > 2) Move();
    }

    void Move()
    {
        golem_action.SetBool("walk", true);
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", golem_trans.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        golem_trans.DOLocalMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - golem_trans.localPosition);
        Vector3 dest_rot = look_rot.eulerAngles;
        golem_trans.DORotate(dest_rot, move_speed);
    }

    IEnumerator Attack()
    {
        golem_action.SetBool("walk", false);
        golem_action.SetBool("attack", true);

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - golem_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        golem_trans.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.7f);
        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - golem_trans.position).sqrMagnitude <= 16)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.6f);
        StartCoroutine("Break");
    }

    IEnumerator Break()
    {
        golem_action.SetBool("attack", false);
        golem_action.SetBool("walk", false);

        yield return new WaitForSeconds(2.0f);

        FindPath();
    }

    IEnumerator Die()
    {
        golem_action.CrossFade("die", 0.3f);
        yield return new WaitForSeconds(1.5f);

        GridManager.instance.SendMessage("ResetObjNode", golem_trans.position);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(golem_trans.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}