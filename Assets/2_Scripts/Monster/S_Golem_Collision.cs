﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class S_Golem_Collision : MonoBehaviour
{
    float health = Define.SGOLEM_HP;
    S_Golem_Animate s_golem_animate;
    float damage;
    public Renderer render;

    void Start()
    {
        health = 30;
        s_golem_animate = GetComponent<S_Golem_Animate>();
        damage = PlayerController.Instance.player_Data.attack_power;
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.CompareTag("Bullet") || other.CompareTag("Explosion"))
        {
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = Define.SGOLEM_HP;
        this.transform.DOKill();
        s_golem_animate.StartCoroutine("Die");
    }
}
