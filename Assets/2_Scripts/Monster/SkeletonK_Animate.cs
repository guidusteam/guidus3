﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class SkeletonK_Animate : MonoBehaviour {

    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    
    Animation skel_anim;
    Boss_Room boss_room;
    
    int move_count = 0;
    float move_speed = 0.3f;
    public bool defence = false;
    int attack_power = 7;

    // Use this for initialization
    void SetTarget()
    {
        skel_anim = GetComponent<Animation>();
        boss_room = GetComponentInParent<Boss_Room>();

        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
    }


    void FindPath()
    {
        if (move_count >= 4)
        {    //4칸 움직이고 defence

            StartCoroutine("Defence");
            return;
        }

        startPos = this.transform;
        endPos = PlayerController.Instance.player_Transform;
        endPos.position = new Vector3(PlayerController.Instance.player_Transform.position.x, 0, PlayerController.Instance.player_Transform.position.z);

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

      
            if (pathArray.Count <= 2)
            {
                StartCoroutine("Attack");
            }
            else Move();
              

    }

    IEnumerator Break()
    {
        skel_anim.CrossFade("idle", 1.0f);
        pathArray.Clear();

        yield return new WaitForSeconds(3.0f);

        move_count = 0;
        FindPath();
    }

    void Move()
    {
        //현재 move동작이 아닐때만 새로 재생
        if (!skel_anim.IsPlaying("run"))
            skel_anim.CrossFade("run", 0.3f);
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        this.transform.DOMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, move_speed);
        move_count++;
    }

    IEnumerator Attack()
    {
        int num = Random.Range(1, 4);   // 1 2 3
        if (!skel_anim.IsPlaying("attack" + num))
            skel_anim.CrossFade("attack" + num, 0.3f);
        move_count = 0;

        Quaternion look_rot = Quaternion.LookRotation(PlayerController.Instance.player_Transform.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.5f);

        //거리 안에 있으면
        if ((PlayerController.Instance.player_Transform.position - this.transform.position).sqrMagnitude < Define.ATTACK_BOUND)
        {
            PlayerController.Instance.player_State.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        skel_anim.CrossFade("die", 0.3f);
        yield return new WaitForSeconds(0.9f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);

        boss_room.CheckBoss(-1);
        this.gameObject.SetActive(false);
    }

    IEnumerator Defence()
    {
        defence = true;
        skel_anim.CrossFade("defence", 0.3f);
        pathArray.Clear();

        yield return new WaitForSeconds(3.0f);

        defence = false;
        move_count = 0;
        FindPath();
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            for (int i = 0; i < pathArray.Count; ++i)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(((Node)pathArray[i]).position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}
