using UnityEngine;
using System.Collections;

// Cartoon FX  - (c) 2015 Jean Moreno

public class CFX_Demo_RandomDirectionTranslate : MonoBehaviour
{
	public float speed = 30.0f;
	public Vector3 baseDir = Vector3.zero;
	public Vector3 axis = Vector3.forward;
	public bool gravity;
	private Vector3 Dir;
	
	void Start ()
	{
		Dir = new Vector3(Random.Range(0.0f,360.0f),Random.Range(0.0f,360.0f),Random.Range(0.0f,360.0f)).normalized;
		Dir.Scale(axis);
		Dir += baseDir;
	}
	
	void Update ()
	{
		this.transform.Translate(Dir * speed * Time.deltaTime);
		
		if(gravity)
		{
			this.transform.Translate(Physics.gravity * Time.deltaTime);
		}
	}
}
